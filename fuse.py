#!/usr/bin/env python


import argparse
import copy
import dicom
import numpy
import os.path
import scipy.ndimage.interpolation
import skimage.color
import skimage.io
import skimage.measure
import skimage.transform


VERBOSE = False


def verbose(function):
    """
    Mark the function as verbose (will not be called if VERBOSE is False).
    """

    def verbose_function(*args, **kwargs):
        if VERBOSE:
            return function(*args, **kwargs)

    return verbose_function


@verbose
def dump(image, name):
    """
    Dump the image to a file with the given name.
    """
    normalized_image = image / float(image.max())
    filename = os.path.join(os.path.dirname(__file__), "dump", name + ".png")
    skimage.io.imsave(filename, normalized_image)


@verbose
def log(message):
    """
    Print the message to the standard output.
    """
    print(message)


def load_dicom(filename):
    """
    Load a DICOM dataset from the file.
    """
    return dicom.read_file(filename)


def save_dicom(dataset, filename):
    """
    Save the DICOM dataset to the file.
    """
    dataset.save_as(filename)


def smallest(dataset):
    """
    Get the smallest pixel value in the dataset.
    """
    if hasattr(dataset, "SmallestImagePixelValue"):
        return dataset.SmallestImagePixelValue
    return 0 # Unsigned storage is assumed.


def largest(dataset):
    """
    Get the largest pixel value in the dataset.
    """
    if hasattr(dataset, "LargestImagePixelValue"):
        return dataset.LargestImagePixelValue
    return 2**dataset.BitsStored - 1 # Unsigned storage is assumed.


def deflate(image, low, high):
    """
    Map the pixel values from [low, high] to [0, 1].
    """
    return (image - low) / float(high - low)


def inflate(image, low, high):
    """
    Map the pixel values from [0, 1] to [low, high].
    """
    return image * (high - low) + low


def pixel_type(dataset):
    """
    Get the pixel storage type of the dataset.
    """
    if dataset.BitsAllocated == 8:
        if dataset.PixelRepresentation == 0:
            return numpy.uint8
        else:
            return numpy.int8
    elif dataset.BitsAllocated == 16:
        if dataset.PixelRepresentation == 0:
            return numpy.uint16
        else:
            return numpy.int16
    raise RuntimeError, "only 8-bit and 16-bit images are supported"


def fetch_image(dataset):
    """
    Fetch an image (deflated, gray or RGB) from the dataset.
    """
    data = numpy.fromstring(dataset.PixelData, dtype=pixel_type(dataset))
    deflated_data = deflate(data, smallest(dataset), largest(dataset))
    shape = (dataset.Rows, dataset.Columns, dataset.SamplesPerPixel)
    image = deflated_data.reshape(shape).squeeze()
    return image


def embed_image(image, dataset):
    """
    Embed the image (deflated, gray or RGB) into the dataset.
    """
    inflated_image = inflate(image, smallest(dataset), largest(dataset))
    data = inflated_image.astype(pixel_type(dataset))
    dataset.PixelData = data.tostring()


def find_roi(image):
    """
    Find the ROI (white rectangular frame) on the image (deflated, RGB).

    Returns the bounding box of the ROI.
    """
    white_area = numpy.all(image == 1, axis=2)
    dump(white_area, "white-area")
    labels = skimage.measure.label(white_area, neighbors=4)
    regions = skimage.measure.regionprops(labels)
    contours = [r for r in regions if 2 * r.area < 5 * r.perimeter]
    border = max(regions, key=lambda r: r.perimeter)
    perimeter = border.perimeter / 2
    regions.remove(border)
    return min(regions, key=lambda r: abs(r.perimeter - perimeter)).bbox


def fetch_roi(image):
    """
    Fetch the content of the ROI of the image (deflated, RGB).
    """
    top, left, bottom, right = find_roi(image)
    roi = image[top : bottom, left : right, :]
    return roi


def hsv(image):
    """
    Convert the RGB image (deflated) into an HSV image.
    """
    return skimage.color.rgb2hsv(image)


def rgb(image):
    """
    Convert the HSV image into an RGB image (deflated).
    """
    return skimage.color.hsv2rgb(image)


def separate(image, transparency):
    """
    Separate the gray and color layers of the image (deflated, RGB).
    """
    hue, saturation, value = hsv(image).transpose(2, 0, 1)
    mask = saturation > 0
    gray_layer = value - mask * (1 - transparency) / transparency * (1 - value)
    color_layer = rgb(numpy.dstack((hue, mask, mask)))
    return gray_layer, color_layer


def register(image, template):
    """
    Align the template to the image (both deflated, gray).

    Returns the scale and the shift (to be applied after scaling).
    """

    def pad(array, shape):
        """
        Reshape the array by padding it with zeros on the bottom and right.
        """
        padding = (shape[0] - array.shape[0], shape[1] - array.shape[1])
        return numpy.pad(array, zip((0, 0), padding), "constant")

    def fft(image):
        """
        Calculate the FFT (2D) of the image.
        """
        return numpy.fft.fftshift(numpy.fft.fft2(image))

    def ifft(image):
        """
        Calculate the inverse FFT (2D) of the image.
        """
        return numpy.fft.ifft2(numpy.fft.ifftshift(image))

    def log_scale(image):
        """
        Log-scale the image.
        """
        return numpy.log(1 + abs(image))

    def log_base(image):
        """
        Calculate the base for the log-polar transform of the square image.
        """
        assert image.shape[0] == image.shape[1]
        size = image.shape[0]
        return 10.0**(numpy.log10(size / numpy.sqrt(2)) / size)

    def log_polar_transform(image, base):
        """
        Calculate the log-polar transform of the square origin-symmetric image.
        """
        assert image.shape[0] == image.shape[1]
        size = image.shape[0]
        angles = -numpy.linspace(0, numpy.pi, size, endpoint=False)
        radii = numpy.power(base, range(size))
        a, r = numpy.meshgrid(angles, radii, indexing="ij")
        y, x = r * numpy.sin(a) + 0.5 * size, r * numpy.cos(a) + 0.5 * size
        return scipy.ndimage.interpolation.map_coordinates(image, (y, x))

    def gradient(image):
        """
        Calculate the modulus of the gradient of the image.
        """
        return numpy.hypot(*numpy.gradient(image))

    def covariation(left, right):
        """
        Calculate the covariation between the two images.
        """
        return abs(ifft(fft(left) * fft(right).conj()))

    def correlation(left, right):
        """
        Calculate the correlation between the two images.

        Takes into account the support of the right image.
        """
        window = (right > 0).astype(float)
        normalization = numpy.sqrt(covariation(left**2, window))
        return covariation(left, right) / normalization

    def estimate_horizontal_shift(left, right):
        """
        Estimate the horizontal shift between the two images.
        """
        return numpy.argmax(correlation(left, right)[0, :])

    def log_weight(image, base):
        """
        Calculate the weighting function for the log-polar transform.
        """
        size = image.shape[0]
        radii = numpy.arange(size)
        soft_threshold = numpy.log(size / (2 * numpy.pi)) / numpy.log(base)
        weight = numpy.exp(-(radii / (radii[-1] - soft_threshold))**2 / 2)
        return weight[None, ::-1].repeat(size, axis=0)

    def estimate_scale(image, template):
        """
        Estimate the scale between the template and the image.

        The images are assumed to be square and have the same size.
        """
        image_spectrum = abs(fft(image))
        dump(log_scale(image_spectrum), "image-spectrum")
        template_spectrum = abs(fft(template))
        dump(log_scale(template_spectrum), "template-spectrum")
        base = log_base(image)
        image_lpt = log_polar_transform(image_spectrum, base)
        template_lpt = log_polar_transform(template_spectrum, base)
        weight = log_weight(image, base)
        image_lpt *= weight
        dump(log_scale(image_lpt), "image-lpt")
        template_lpt *= weight
        dump(log_scale(template_lpt), "template-lpt")
        return base**(-estimate_horizontal_shift(image_lpt, template_lpt))

    def rescale(image, scale):
        """
        Scales the image preserving its shape.
        """
        shape = image.shape
        scaled_image = skimage.transform.rescale(image, scale)
        return pad(scaled_image[:shape[0], :shape[1]], shape)

    def estimate_shift(image, template):
        """
        Estimate the shift between the template and the image.
        """
        peak = correlation(image, template).argmax()
        return numpy.unravel_index(peak, image.shape)

    size = 2 * max(image.shape + template.shape)
    shape = (size, size)
    image_gradient = pad(gradient(image), shape)
    dump(image_gradient, "image-gradient")
    template_gradient = pad(gradient(template), shape)
    dump(template_gradient, "template-gradient")
    scale = estimate_scale(image_gradient, template_gradient)
    scaled_template_gradient = rescale(template_gradient, scale)
    dump(scaled_template_gradient, "scaled-template-gradient")
    shift = estimate_shift(image_gradient, scaled_template_gradient)
    return scale, shift


def paste(source, destination, scale, shift, transparency=0):
    """
    Paste the source to the destination image (both deflated, gray or RGB).
    """
    scaled_source = skimage.transform.rescale(source, scale)
    y, x = shift
    h, w = scaled_source.shape[:2]
    region = (slice(y, y + h), slice(x, x + w))
    opacity = 1 - transparency
    if len(source.shape) == 3:
        mask = opacity * numpy.any(scaled_source, axis=2, keepdims=True)
        destination[region] += mask * (scaled_source - destination[region])
    else:
        destination[region] += opacity * (scaled_source - destination[region])


def derive_dicom(dataset, image, settings):
    """
    Derive a new dataset from the given one holding the image (deflated, gray).
    """
    new_dataset = copy.deepcopy(dataset)
    uid_suffix = ".26"
    new_dataset.file_meta.MediaStorageSOPInstanceUID += uid_suffix
    new_dataset.file_meta.ImplementationClassUID += uid_suffix
    new_dataset.SOPInstanceUID += uid_suffix
    new_dataset.ImageType = ["DERIVED", "SECONDARY", "MR"]
    new_dataset.SeriesInstanceUID += uid_suffix
    new_dataset.SeriesNumber = str(settings.series_number)
    new_dataset.InstanceNumber = str(settings.instance_number)
    new_dataset.ImagesInAcquisition = str(settings.images_in_acquisition)
    new_dataset.InStackPositionNumber = settings.instance_number
    new_dataset.SamplesPerPixel = 1
    new_dataset.PhotometricInterpretation = "MONOCHROME2"
    new_dataset.PixelRepresentation = 0
    new_dataset.BitsAllocated = 8
    new_dataset.BitsStored = 8
    new_dataset.HighBit = 7
    new_dataset.SmallestImagePixelValue = 0
    new_dataset.LargestImagePixelValue = 255
    new_dataset.WindowCenter = 128
    new_dataset.WindowWidth = 256
    embed_image(image, new_dataset)
    return new_dataset


def main(args):
    global VERBOSE
    VERBOSE = args.verbose
    mri_data = load_dicom(args.mri)
    mri = fetch_image(mri_data)
    dump(mri, "mri")
    mrs_data = load_dicom(args.mrs)
    mrs = fetch_image(mrs_data)
    dump(mrs, "mrs")
    roi = fetch_roi(mrs)
    dump(roi, "roi")
    roi_gray, roi_color = separate(roi, args.transparency)
    dump(roi_gray, "roi-gray")
    dump(roi_color, "roi-color")
    scale, shift = register(mri, roi_gray)
    log((scale, shift))
    aligned_roi_gray = numpy.copy(mri)
    paste(roi_gray, aligned_roi_gray, scale, shift)
    dump(aligned_roi_gray, "aligned-roi-gray")
    roi_hue, roi_saturation, roi_value = hsv(roi_color).transpose(2, 0, 1)
    roi_index = roi_value * (1 - roi_hue)
    dump(roi_index, "roi-index")
    aligned_roi_index = numpy.zeros(mri.shape)
    paste(roi_index, aligned_roi_index, scale, shift, args.transparency)
    dump(aligned_roi_index, "aligned-roi-index")
    output_data = derive_dicom(mri_data, aligned_roi_index, args)
    save_dicom(output_data, args.output)


if __name__ == "__main__":
    description = "align spectroscopy data to MRI data"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-i", "--mri",
        required=True,
        help="input DICOM (MR) file with MRI data")
    parser.add_argument("-s", "--mrs",
        required=True,
        help="input DICOM (SCREENSAVE) file with spectroscopy data")
    parser.add_argument("-o", "--output",
        required=True,
        help="output DICOM (MR) file with aligned spectroscopy data")
    parser.add_argument("-t", "--transparency",
        type=float,
        default=0.5,
        help="transparency of the color map (default is 0.5)")
    parser.add_argument("--series-number",
        default="1",
        help="`series number` tag of the output (default is 1)")
    parser.add_argument("--instance-number",
        type=int,
        default=1,
        help="`instance number` tag of the output (default is 1)")
    parser.add_argument("--images-in-acquisition",
        default="1",
        help="`images in acquisition` tag of the output (default is 1)")
    parser.add_argument("-v", "--verbose",
        action="store_true",
        help="output debugging information")
    main(parser.parse_args())
