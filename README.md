# MRS Fusion

Python script for aligning rendered MR spectroscopy data to MR images.

---

## License

This project is released under the MIT license.

---

## Requirements

You need to have the following packages be installed on your system:

* [pydicom](http://www.pydicom.org/) for handling DICOMs;
* [numpy](http://www.numpy.org/) for efficient array processing;
* [scipy](http://www.scipy.org/) and [scikit-image](http://scikit-image.org/)
for image processing.

---

## Usage

You can always run
```
#!console
$ ./fuse.py --help
```
to get the list of available arguments.
Here is an example:
```
#!console
$ ./fuse.py -i example/mri.dcm -s example/mrs.dcm -o example/fusion.dcm -v
```
| `mri.dcm` | `mrs.dcm` | `fusion.dcm` |
|:---:|:---:|:---:|
| ![mri](http://bitbucket.org/thoughteer/mrs-fusion/raw/master/example/mri.png) | ![mrs](http://bitbucket.org/thoughteer/mrs-fusion/raw/master/example/mrs.png) | ![fusion](http://bitbucket.org/thoughteer/mrs-fusion/raw/master/example/fusion.png) |